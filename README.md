# README #

Hack done during Copenhacks2017 http://copenhacks.com/
By:
	- Miguel Fontes
	- Dennis Madsen

### What it does
Allows users with visual impairments to share photos on social media and with friends through MMS. The user is able to take a picture and the app will read the content of the image back to the user. The user can then decide to share the image to Twitter, send it as an MMS or simply take a new photo to get to know the environment.

### How we built it
The application is using Microsoft cognitive services to figure out what the picture is showing. The Twilio API is used to send text messages. The application is created as a web-application using Flask. Jquery libraries are used to allow for touch interface gestures.

### Challenges we ran into
The HTML5 camera module was far from as mature as we had hoped for. During the night we started porting the app to Android. It was though limited how far we got with this due to our limited knowledge in the area.

### What's next for SocialEyes
 - Implement speech to text to send SMS and MMS to friends and family.
 - Add user information from selfie picture
 - Voice interaction navigation
 - Make it faster!!!