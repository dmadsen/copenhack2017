'''
Created on Apr 22, 2017

@author: pfontes
'''

# https://www.microsoft.com/cognitive-services/en-us/computer-vision-api
msCvKey       = r'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX' #Computer Vision - Free
# https://www.microsoft.com/cognitive-services/en-us/speech-api
bingSpeechKey = r'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX' #Bing Speech - Free
# https://www.microsoft.com/cognitive-services/en-us/speaker-recognition-api
msSpeakKey    = r'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX' #Speaker Recognition - Preview

# https://www.twilio.com/docs/api/rest/sending-messages
twilio_account_sid  = r'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
twilio_auth_token   = r'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
#
TWILIO_PHONE_SENDER        = '+10000000000'
TWILIO_PHONE_RECEIVER      = ['+1000000000']

# https://dev.twitter.com/rest/public
TWITTER_API_KEY            = r'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
TWITTER_API_SECRET         = r'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
TWITTER_OAUTH_TOKEN        = r'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
TWITTER_OAUTH_TOKEN_SECRET = r'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'

