'''
Created on Apr 22, 2017

@author: pfontes
'''

from flask import Flask, render_template, request, send_from_directory, redirect, url_for
from flask_sslify import SSLify
import os
import hashlib
import random
import traceback

from modules.twitterApi import TwitterAPI
from modules.twilioAPI import Twilio
from modules.microsoftApi import ComputerVision
from msspeak import msspeak

from modules.sandbox import imageDescriptionToText as img2txt

from keys import msSpeakKey, msCvKey, bingSpeechKey, TWILIO_PHONE_SENDER, TWILIO_PHONE_RECEIVER

tags = {}
captions = {}

twitter = TwitterAPI()
tw = Twilio()

context = ('../cert.pem', '../key.pem')
app = Flask(__name__, static_folder="./", template_folder="./")
sslify = SSLify(app, subdomains=True)
app.config['UPLOAD_FOLDER'] = "./pics"

@app.route('/js/<path:path>')
def js(path):
    return send_from_directory('js', path)

@app.route('/css/<path:path>')
def css(path):
    return send_from_directory('css', path)

@app.route('/mp3/<path:path>')
def mp3(path):
    return send_from_directory('mp3', path)

@app.route('/static/<path:path>')
def get_static(path):
    return send_from_directory('static', path)

@app.route('/pics/<path:path>')
def pics(path):
    return send_from_directory('pics', path)

@app.route('/capture')
def capture():
    return render_template("capture.html")

@app.route('/twitter', methods=['POST'])
def twitter_api():
    try:
        
        caption = "@CopenHacks #copenhacks #SocialEyes"
        
        caption += request.form['caption']
        
        filename = request.form['filename'].split(".")[0]

        
        for tag in tags[filename][:5]:
            caption += " #%s" %tag
        
        pic = "%s.jpg" %filename
        twitter.tweet(caption, open("./pics/%s" %pic, "rb"))
    except:
        return "0"
    
    return "1"

@app.route('/mms', methods=['POST'])
def mms():
    
    filename = request.form['filename'].split(".")[0]
    
    phone_list = TWILIO_PHONE_RECEIVER

    for to in phone_list:
        tw.sendSMS(to, TWILIO_PHONE_SENDER , captions[filename])
    
    return "1"


@app.route('/snapshot', methods=['POST'])
def snapshot():
    
    if not os.path.exists('./pics'): os.mkdir('./pics')
    if not os.path.exists('./mp3'): os.mkdir('./mp3')
    
    try:
        _file = request.files['webcam']
        filename = "%s" %hashlib.sha1("%s" %random.random()).hexdigest()[:12]
        _file.save(os.path.join(app.config['UPLOAD_FOLDER'], "%s.jpg" %filename))
        
        mscv = ComputerVision(msCvKey)
        
        with open('./pics/%s.jpg' %filename, 'rb') as fh:
            res = mscv.resultFromImage(fh.read())
            
            tmp_tags = res['tags']
            
            res = img2txt(res['captions'])
            
            print(res)
            
        tts_msspeak = msspeak.Speech(bingSpeechKey)
        
        mp3_filename = "%s.mp3" %filename
        
        captions[filename] = res
        tags[filename] = tmp_tags
        
        with open("mp3/%s" %mp3_filename, 'wb') as fh:            
            tts_msspeak.speak_to_file(fh, res, "en-GB", gender="female", format='riff-8khz-8bit-mono-mulaw')

    except Exception:
        traceback.print_exc()
    
    return mp3_filename

if __name__ == "__main__":
    app.run('0.0.0.0', port=5000, debug=False, ssl_context=context)