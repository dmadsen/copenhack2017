'''
Created on Apr 22, 2017

@author: pfontes
'''

def imageDescriptionToText(descriptions):
    outText = 'In the image you see: '
    for i, desc in enumerate(descriptions):
        if i>0:
            outText += " In the image you also see: "
        outText += desc['text'] + \
                   '. I am ' + "%.0f percent confident about this." % \
                               (float(desc['confidence']) * 100)
    return outText