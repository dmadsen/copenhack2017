'''
Created on Apr 23, 2017

@author: pfontes
'''

from keys import TWITTER_API_KEY, TWITTER_API_SECRET, TWITTER_OAUTH_TOKEN, TWITTER_OAUTH_TOKEN_SECRET

from twython import Twython

class TwitterAPI():
    
    def __init__(self):
        self.twitter = Twython(TWITTER_API_KEY, TWITTER_API_SECRET, TWITTER_OAUTH_TOKEN, TWITTER_OAUTH_TOKEN_SECRET)
        
    def tweet(self, _status, _img=None):
        
        if _img:
            response = self.twitter.upload_media(media=_img)
            return self.twitter.update_status(status=_status, media_ids=[response['media_id']])
            
        self.twitter.update_status(status=_status)

#twitter = TwitterAPI()

#twitter.tweet("Cool image upload", open("../pics/17b27b1e12b2.jpg", "rb"))