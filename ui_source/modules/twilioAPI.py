'''
Created on Apr 23, 2017

@author: pfontes
'''

from twilio.rest import Client
from keys import twilio_account_sid, twilio_auth_token
# put your own credentials here


class Twilio():
    
    def __init__(self):
        self.client = Client(twilio_account_sid, twilio_auth_token)

    def sendSMS(self, _to, _from, _msg):

        self.client.messages.create(
                to= _to,
                from_= _from,
                body="SocialEyes image description: %s " %_msg)

"""
tw = Twilio()

phone_list = ['+4500000000']


for to in phone_list:
    tw.sendSMS(to, "+10000000000" , "large dog running your direction")
"""